#### **Project Title: My_hello_world**

My_hello_world is a simple project written in Kotlin for different Android devices.


#### **Prerequisites:**

This project will work on all Android devices, having minimum SDK 17. Default app language is English.


#### **Description:**

Users speaking Russian, Belarusian, Ukrainian, Spanish, German, Italian, Polish and French will appreciate this application in their native language.

This app also helps users to identify their Android version: if it's 7.0 and higher, they will see the text "Hello World" in blue, otherwise - in red.


#### **Installation**

Clone or download .zip file of the repository <https://bitbucket.org/katerina_kniazeva/my_hello_world.git>

  git clone https://Katerina_Kniazeva@bitbucket.org/katerina_kniazeva/my_hello_world.git

and install on your device. You also need to have Android Studio installed on your computer. 


#### **Importing into Android Studio**

Open Android Studio and select **Open an Existing Android Studio Project** or **File, Open.**
Locate the folder you downloaded from bitbucket and unzipped, choosing the “build.gradle” file in the root directory.

Select **Project** on the left side to view and explore the files in your app.

Open the **“res/layout”** directory to access the UI configuration for the app and other layout components.

Click the **Run** button to run the app – if you have not already created an Android emulator, Android Studio will prompt you to do so at this point.

Once you have an emulator, select it to launch your app on it. 

If you have a physical device, to run the app, connect it to your computer and choose it in the dropdown menu next to **"Run"** button.
